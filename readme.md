# Fabulous Paintings

This Painting Website contains all the artwork which I am doing since my childhood. I used to play with water paints but by the passage of time I started working in mixed media too. Please check my website.

I usually inspired by nature and paint gardens, birds, butterflies, roses, lilies, and jasmines. I try to play with whites in combination of other soft and light shades of pinks, yellows, greens, blues, and purples. I also try to create out of mud and clay but I think paintings are my soul creations. Recently, I have joined an academy learning oil canvas art.

Eartha Benns -- Assistant Designer at http://www.blclawcenter.com/