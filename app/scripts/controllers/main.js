'use strict';

/**
 * @ngdoc function
 * @name paintingsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the paintingsApp
 */
angular.module('paintingsApp')
  .controller('MainCtrl', function ($scope, $filter, $interval, $timeout, AppSettings) {


    // scroll thing
    angular.element('.painting').imagesLoaded(function() {
      var windowEl = angular.element(window);
      var windowHeight = windowEl.height();
      var bodyHeight = angular.element('body').height();
      var scrollRemain = bodyHeight - windowHeight;
      var photoHeight = 1333;
      var pr = 0;
      angular.element(window).scroll(function() {
        pr = Math.round(windowEl.scrollTop() * 100 / scrollRemain);
        //console.log(pr);
      });
    });


  });
