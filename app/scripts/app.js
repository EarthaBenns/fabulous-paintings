'use strict';

/**
 * @ngdoc overview
 * @name paintingsApp
 * @description
 * # paintingsApp
 *
 * Main module of the application.
 */
angular
  .module('paintingsApp', ['ngAnimate', 'ngResource', 'ngSanitize', 'ngTouch', 'ngRoute'])
  .config(function ($routeProvider, $locationProvider) {
    //$locationProvider.html5Mode(true);
    $routeProvider
      .when('/', {
        templateUrl: 'views/home.html',
        controller: 'MainCtrl',
        action: 'home'
      })
      .when('/:author', {
        templateUrl: 'views/author.html',
        controller: 'AuthorCtrl',
        action: 'view'
      })
      .when('/:author/:painting', {
        templateUrl: 'views/painting.html',
        controller: 'PaintingCtrl',
        action: 'view'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .constant('AppSettings', {
    apiUrl: '/api/',
  });
